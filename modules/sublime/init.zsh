# Sublime Text 2 Aliases

local _sublime_darwin_paths > /dev/null 2>&1
_sublime_darwin_paths=(
    "/usr/local/bin/subl"
    "$HOME/Applications/Sublime Text 3.app/Contents/SharedSupport/bin/subl"
    "$HOME/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl"
    "$HOME/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl"
    "/Applications/Sublime Text 3.app/Contents/SharedSupport/bin/subl"
    "/Applications/Sublime Text 2.app/Contents/SharedSupport/bin/subl"
    "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl"
)

local _sublime_cygwin_paths > /dev/null 2>&1
_sublime_cygwin_paths=(
    "/cygdrive/c/Program Files/Sublime Text 3/sublime_text.exe"
    "/cygdrive/c/Program Files/Sublime Text 2/sublime_text.exe"
    "/cygdrive/c/Program Files/Sublime Text/sublime_text.exe"
    "/cygdrive/c/Program Files (x86)/Sublime Text 3/sublime_text.exe"
    "/cygdrive/c/Program Files (x86)/Sublime Text 2/sublime_text.exe"
    "/cygdrive/c/Program Files (x86)/Sublime Text/sublime_text.exe"
)

if [[ $('uname') == 'Linux' ]]; then
    if [ -f '/usr/bin/sublime_text' ]; then
        st_run() { nohup /usr/bin/sublime_text $@ > /dev/null & }
    else
        st_run() { nohup /usr/bin/sublime-text $@ > /dev/null & }
    fi
    alias st=st_run

elif  [[ $('uname') == 'Darwin' ]]; then

    for _sublime_path in $_sublime_darwin_paths; do
        if [[ -a $_sublime_path ]]; then
            alias subl="'$_sublime_path'"
            alias st=subl
            break
        fi
    done

elif [[ $('uname') == *CYGWIN* ]]; then

    for _sublime_path in $_sublime_cygwin_paths; do
        if [[ -a $_sublime_path ]]; then
            _selected_cygwin_path="$_sublime_path"
            break
        fi
    done

    st_run() { nohup $_selected_cygwin_path $@ > /dev/null & }
    alias st=st_run

fi

alias stt='st .'
