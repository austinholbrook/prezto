Sublime
=======

Adds aliases for sublime text (1, 2 or 3) on Mac, Linux and Cygwin

Authors
-------

This module was based on the sublime plugin for oh-my-zsh, and patched to include cygwin support.

*The authors of this module should be contacted via the [issue tracker][1].*

  - [Austin Holbrook](https://github.com/ausholbrook)

[1]: https://github.com/ausholbrook/prezto/issues

